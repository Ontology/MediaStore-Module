﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MediaStore_Module
{
    
    public class FileWorkManager : NotifyPropertyChange
    {
        private OntologyModDBConnector dbConnectorBlob;
        private OntologyModDBConnector dbConnectorServer;
        private OntologyModDBConnector dbConnectorShare;
        private OntologyModDBConnector dbConnectorDrive;
        private OntologyModDBConnector dbConnectorFolder;
        private OntologyModDBConnector dbConnectorFileSystemObject;
        
        private clsLocalConfig localConfig;
        private clsTransaction transactionManager;
        private clsRelationConfig relationConfigurator;

        public clsOntologyItem ResultGetFileSystemObject { get; private set; }

        public MediaStoreConnector MediaStoreConnector { get; private set; }

        public clsLocalConfig LocalConfig
        {
            get
            {
                return localConfig;
            }
        }

        private enum PathTypeEnum
        {

            None = 0,
            ServerPath = 1,
            LocalPath = 2,

        }

        public bool IsBlobFile(clsOntologyItem oItemFile)
        {
            return IsBlobFile(oItemFile.GUID);
        }

        public bool IsBlobFile(string guidFile)
        {
            var searchBlob = new List<clsObjectAtt> { new clsObjectAtt
                {
                    ID_Object = guidFile,
                    ID_AttributeType = localConfig.OItem_attributetype_blob.GUID
                }
            };

            var result = dbConnectorBlob.GetDataObjectAtt(searchBlob);

            var valBlob = dbConnectorBlob.ObjAtts.FirstOrDefault();

            return (valBlob != null ? valBlob.Val_Bit != null ? valBlob.Val_Bit.Value : false : false);
        }

        public clsOntologyItem GetFileItem(string idFile, string nameFile)
        {
            return new clsOntologyItem
            {
                GUID = idFile,
                Name = nameFile,
                GUID_Parent = localConfig.OItem_class_file.GUID,
                Type = localConfig.Globals.Type_Object
            };
        }

        public clsOntologyItem CreateFile(string fileName)
        {
            var fileSystemObject = new clsOntologyItem
            {
                GUID = localConfig.Globals.NewGUID,
                Name = fileName,
                GUID_Parent = localConfig.OItem_class_file.GUID,
                Type = localConfig.Globals.Type_Object
            };

            transactionManager.ClearItems();
            var result = transactionManager.do_Transaction(fileSystemObject);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result.OList_Rel = new List<clsOntologyItem>();
                result.OList_Rel.Add(fileSystemObject);
            }

            return result;
        }

        public FileInfo GetFileInfo(clsOntologyItem objOItem_File)
        {
            string path = "";
            if (IsBlobFile(objOItem_File))
            {
                var mediaStoreConnector = new MediaStoreConnector(localConfig.Globals);
                path = mediaStoreConnector.GetMediaBlobPath(objOItem_File);

            }
            else
            {
                path = GetPathFileSystemObject(objOItem_File);
            }

            if (!string.IsNullOrEmpty(path) && File.Exists(path))
            {
                var fileInfo = new FileInfo(path);
                return fileInfo;
            }
            else
            {
                return null;
            }
        }

        public clsOntologyItem GetFileSystemObjectByPath(string path, bool doCreate)
        {
            var result = localConfig.Globals.LState_Error.Clone();
            var oItemFileSystemObject = new clsOntologyItem
            {
                Type = localConfig.Globals.Type_Object
            };

            if (File.Exists(path))
            {
                oItemFileSystemObject.Name = Path.GetFileName(path);
                oItemFileSystemObject.GUID_Parent = localConfig.OItem_class_file.GUID;
                result = localConfig.Globals.LState_Success.Clone();
            }
            else if (Directory.Exists(path))
            {
                oItemFileSystemObject.Name = Path.GetFileName(path);
                oItemFileSystemObject.GUID_Parent = localConfig.OItem_class_folder.GUID;
                result = localConfig.Globals.LState_Success.Clone();
            }

            PathTypeEnum pathType = PathTypeEnum.None;
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                clsOntologyItem shareItem = null;
                clsOntologyItem driveItem = null;

                path = path.Substring(0, path.Length - oItemFileSystemObject.Name.Length);
                var folders = path.Split('\\');

                if (PathContainsServer(path))
                {
                    pathType = PathTypeEnum.ServerPath;
                    result = GetServerFromPath(path, doCreate);
                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        result = GetShareFromPath(path, doCreate, result.OList_Rel.First());
                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            shareItem = result.OList_Rel.First();
                        }
                    }
                }
                else if (path.Contains(":"))
                {
                    pathType = PathTypeEnum.LocalPath;

                    result = GetDriveFromPath(path, doCreate);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        driveItem = result.OList_Rel.First();
                    }

                }
                else
                {
                    result = localConfig.Globals.LState_Error.Clone();
                }

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var ixStart = pathType == PathTypeEnum.LocalPath ? 1 : 4;

                    transactionManager.ClearItems();

                    for (var ix = ixStart;ix < folders.Length; ix++)
                    {
                        var folder = folders[ix];
                        if (!string.IsNullOrEmpty(folder))
                        {
                            result = CheckSubFolder(pathType == PathTypeEnum.LocalPath ? driveItem : shareItem, folder, doCreate, transactionManager);
                            if (result.GUID == localConfig.Globals.LState_Error.GUID)
                            {
                                transactionManager.rollback();
                                break;
                            }
                            else if (result.GUID == localConfig.Globals.LState_Nothing.GUID)
                            {
                                break;
                            }
                        }
                        
                    } 

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        var lastFolder = result.OList_Rel.First();
                        var searchFileSystemObject = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Parent_Object = oItemFileSystemObject.GUID_Parent,
                                ID_RelationType = localConfig.OItem_relationtype_is_subordinated.GUID,
                                ID_Other = lastFolder.GUID
                            }
                        };

                        result = dbConnectorFileSystemObject.GetDataObjectRel(searchFileSystemObject);

                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var searchedFileSystemObject = dbConnectorFileSystemObject.ObjectRels.Where(fso => fso.Name_Object.ToLower() == oItemFileSystemObject.Name.ToLower()).Select(fso => new clsOntologyItem
                            {
                                GUID = fso.ID_Object,
                                Name = fso.Name_Object,
                                GUID_Parent = fso.ID_Parent_Object,
                                Type = localConfig.Globals.Type_Object
                            }).FirstOrDefault();

                            if (searchedFileSystemObject == null)
                            {
                                oItemFileSystemObject.GUID = localConfig.Globals.NewGUID;

                                if (doCreate)
                                {
                                    result = transactionManager.do_Transaction(oItemFileSystemObject);
                                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                                    {
                                        var relFileSystemItemToFolder = relationConfigurator.Rel_ObjectRelation(oItemFileSystemObject, lastFolder, localConfig.OItem_relationtype_is_subordinated);

                                        result = transactionManager.do_Transaction(relFileSystemItemToFolder);

                                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                                        {
                                            result.add_OItem(oItemFileSystemObject);
                                        }
                                        else
                                        {
                                            transactionManager.rollback();
                                        }
                                    }
                                    else
                                    {
                                        transactionManager.rollback();
                                    }
                                    
                                }
                                else
                                {
                                    result = localConfig.Globals.LState_Nothing.Clone();
                                }
                                
                            }
                            else
                            {
                                oItemFileSystemObject = searchedFileSystemObject;
                                result.add_OItem(oItemFileSystemObject);
                            }
                            
                        }
                        else
                        {
                            transactionManager.rollback();
                        }

                        
                    }
                }
                
            }

            var fileSystemObjectToReturn = result.OList_Rel.FirstOrDefault();
            ResultGetFileSystemObject = result.Clone();
            return fileSystemObjectToReturn;
        }

        public string GetPathFileSystemObject(clsOntologyItem oItemFileSystemObject, bool blobPath = false)
        {
            var path = "";
            var seperator = Path.DirectorySeparatorChar;
            ResultGetFileSystemObject = localConfig.Globals.LState_Nothing.Clone();

            if (oItemFileSystemObject.GUID_Parent == localConfig.OItem_class_server.GUID)
            {
                path = seperator + seperator + oItemFileSystemObject.Name;
            }
            else if (oItemFileSystemObject.GUID_Parent == localConfig.OItem_class_drive.GUID)
            {
                path = oItemFileSystemObject.Name;
                if (!oItemFileSystemObject.Name.EndsWith(":"))
                {
                    path += ":";
                }
                
            }
            else if (oItemFileSystemObject.GUID_Parent == localConfig.OItem_class_folder.GUID)
            {
                clsOntologyItem rootItem = null;
                var folderPaths = GetParentFolderPath(oItemFileSystemObject);
                var pathMerge = "";
                if (folderPaths.Any())
                {
                    pathMerge = string.Join(Path.DirectorySeparatorChar.ToString(), folderPaths.Where(folderItem => folderItem != null).Select(folderItem => folderItem.Name).ToArray());
                    rootItem = GetParentServerOrDrive(folderPaths.First());
                }
                else
                {
                    rootItem = GetParentServerOrDrive(oItemFileSystemObject);
                }

                pathMerge = pathMerge + (!string.IsNullOrEmpty(pathMerge) ? Path.DirectorySeparatorChar.ToString() : "") + oItemFileSystemObject.Name;

                if (rootItem != null)
                {
                    if (rootItem.GUID_Parent == localConfig.OItem_class_drive.GUID)
                    {
                        pathMerge = rootItem.Name + (rootItem.Name.EndsWith(":") ? "" : ":") + pathMerge;
                    }
                    else
                    {
                        pathMerge = Path.DirectorySeparatorChar.ToString() + Path.DirectorySeparatorChar.ToString() + rootItem.Name + Path.DirectorySeparatorChar + pathMerge;
                    }
                }

                ResultGetFileSystemObject = localConfig.Globals.LState_Success.Clone();
                path = pathMerge;
            }
            else if (oItemFileSystemObject.GUID_Parent == localConfig.OItem_class_file.GUID)
            {
                if (blobPath)
                {
                    var searchBlob = new List<clsObjectAtt>
                    {
                        new clsObjectAtt
                        {
                            ID_Object = oItemFileSystemObject.GUID,
                            ID_AttributeType = localConfig.OItem_attributetype_blob.GUID
                        }
                    };

                    var result = dbConnectorBlob.GetDataObjectAtt(searchBlob);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        if (dbConnectorBlob.ObjAtts.Any())
                        {
                            if (dbConnectorBlob.ObjAtts.First().Val_Bit != null && dbConnectorBlob.ObjAtts.First().Val_Bit.Value == true)
                            {
                                path = localConfig.Globals.Index + "@" + localConfig.Globals.Server + ":" + oItemFileSystemObject.GUID;
                            }
                            else
                            {
                                ResultGetFileSystemObject = localConfig.Globals.LState_Nothing.Clone();
                            }

                        }
                        else
                        {
                            ResultGetFileSystemObject = localConfig.Globals.LState_Nothing.Clone();
                        }
                
                    }
                    else
                    {
                        ResultGetFileSystemObject = result.Clone();
                    }
                }
                else
                {
                    clsOntologyItem rootItem = null;
                    var folderPaths = GetParentFolderPath(oItemFileSystemObject);
                    var pathMerge = "";
                    if (folderPaths.Any())
                    {
                        pathMerge = string.Join(Path.DirectorySeparatorChar.ToString(), folderPaths.Where(folderItem => folderItem != null).Select(folderItem => folderItem.Name).ToArray());
                        rootItem = GetParentServerOrDrive(folderPaths.First());
                    }
                    else
                    {
                        rootItem = GetParentServerOrDrive(oItemFileSystemObject);
                    }

                    pathMerge = pathMerge + (!string.IsNullOrEmpty(pathMerge) ? Path.DirectorySeparatorChar.ToString() : "") + oItemFileSystemObject.Name;

                    if (rootItem != null)
                    {
                        if (rootItem.GUID_Parent == localConfig.OItem_class_drive.GUID)
                        {
                            pathMerge = rootItem.Name + (rootItem.Name.EndsWith(":") ? "" : ":") + pathMerge;
                        }
                        else
                        {
                            pathMerge = Path.DirectorySeparatorChar.ToString() + Path.DirectorySeparatorChar.ToString() + rootItem.Name + Path.DirectorySeparatorChar + pathMerge;
                        }
                    }

                    ResultGetFileSystemObject = localConfig.Globals.LState_Success.Clone();
                    path = pathMerge;
                }
                
            }
            else
            {
                ResultGetFileSystemObject = localConfig.Globals.LState_Error.Clone();
            }


            return path;
        }

        public string MergePaths(string path1, string path2)
        {
            if (path1.EndsWith(@"\") || path2.StartsWith(@"\"))
            {
                return path1 + path2;
            }
            else
            {
                return path1 + Path.DirectorySeparatorChar + path2;
            }
        }

        public clsOntologyItem GetParentServerOrDrive(clsOntologyItem oItemFileSystemObject)
        {
            clsOntologyItem parentItem = null;
            var searchDrive = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemFileSystemObject.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_is_subordinated.GUID,
                    ID_Parent_Other = localConfig.OItem_class_drive.GUID
                }
            };

            var result = dbConnectorDrive.GetDataObjectRel(searchDrive);

            ResultGetFileSystemObject = result.Clone();

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                parentItem = dbConnectorDrive.ObjectRels.Select(driveItem => new clsOntologyItem
                {
                    GUID = driveItem.ID_Other,
                    Name = driveItem.Name_Other,
                    GUID_Parent = driveItem.ID_Parent_Other,
                    Type = localConfig.Globals.Type_Object
                }).FirstOrDefault();
                
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID && parentItem == null)
            {
                var searchServer = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = oItemFileSystemObject.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_fileshare.GUID,
                        ID_Parent_Object = localConfig.OItem_class_server.GUID
                    }
                };

                result = dbConnectorServer.GetDataObjectRel(searchServer);

                ResultGetFileSystemObject = result.Clone();

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    parentItem = dbConnectorServer.ObjectRels.Select(serverItem => new clsOntologyItem
                    {
                        GUID = serverItem.ID_Object,
                        Name = serverItem.Name_Object,
                        GUID_Parent = serverItem.ID_Parent_Object,
                        Type = localConfig.Globals.Type_Object
                    }).FirstOrDefault();
                }
            }

            return parentItem;
        }

        public List<clsOntologyItem> GetParentFolderPath(clsOntologyItem subItem)
        {
            var resultItem = new List<clsOntologyItem>();
            var searchParentFolder = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = subItem.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_is_subordinated.GUID,
                    ID_Parent_Other = localConfig.OItem_class_folder.GUID
                }
            };

            var result = dbConnectorFolder.GetDataObjectRel(searchParentFolder);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (dbConnectorFolder.ObjectRels.Any())
                {
                    var parentItem = dbConnectorFolder.ObjectRels.Select(parFolder => new clsOntologyItem
                    {
                        GUID = parFolder.ID_Other,
                        Name = parFolder.Name_Other,
                        GUID_Parent = parFolder.ID_Parent_Other,
                        Type = localConfig.Globals.Type_Object
                    }).First();

                    resultItem.Add(parentItem);

                    var pathSuperordinate = GetParentFolderPath(parentItem);
                    resultItem.InsertRange(0, pathSuperordinate);
                    
                }


            }

            return resultItem;
        }

        public clsOntologyItem CheckSubFolder(clsOntologyItem parentItem, string subFolder, bool createSubFolder, clsTransaction transactionManager)
        {
            var searchSubFolder = new List<clsObjectRel>
            { new clsObjectRel
                {
                    ID_Other = parentItem.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_is_subordinated.GUID,
                    ID_Parent_Object = localConfig.OItem_class_folder.GUID    
                }
            };

            clsOntologyItem subFolderItem = null;
            var result = dbConnectorFolder.GetDataObjectRel(searchSubFolder);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                subFolderItem = dbConnectorFolder.ObjectRels.Where(fold => fold.Name_Object == subFolder).Select(fold => new clsOntologyItem
                {
                    GUID = fold.ID_Object,
                    Name = fold.Name_Object,
                    GUID_Parent = fold.ID_Parent_Object,
                    Type = localConfig.Globals.Type_Object
                }).FirstOrDefault();

                if (subFolderItem == null)
                {
                    subFolderItem = new clsOntologyItem
                    {
                        GUID = localConfig.Globals.NewGUID,
                        Name = subFolder,
                        GUID_Parent = localConfig.OItem_class_folder.GUID,
                        Type = localConfig.Globals.Type_Object
                    };

                    result = transactionManager.do_Transaction(subFolderItem);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        var relSubFolderToFolder = relationConfigurator.Rel_ObjectRelation(subFolderItem, parentItem, localConfig.OItem_relationtype_is_subordinated);
                        result = transactionManager.do_Transaction(relSubFolderToFolder);
                    }
                }


            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID && subFolderItem != null)
            {
                result.add_OItem(subFolderItem);
            }

            return result;
        }

        public bool PathContainsServer(string path)
        {
            return path.StartsWith(@"\\");
        }

        public clsOntologyItem GetDriveFromPath(string path, bool createDrive)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var folders = path.Split('\\');

            if (folders[0].EndsWith(":"))
            {
                var driveName = folders[0];
                var searchDrive = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = localConfig.Globals.OItem_Server.GUID,
                        Name_Object = driveName,
                        ID_Parent_Object = localConfig.OItem_class_drive.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_is_subordinated.GUID
                    }
                };

                result = dbConnectorDrive.GetDataObjectRel(searchDrive);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var driveItem = dbConnectorDrive.ObjectRels.Where(drive => drive.Name_Object.ToLower() == driveName.ToLower()).Select(drive => new clsOntologyItem
                    {
                        GUID = drive.ID_Object,
                        Name = drive.Name_Object,
                        GUID_Parent = localConfig.OItem_class_drive.GUID,
                        Type = localConfig.Globals.Type_Object
                    }).FirstOrDefault();

                    if (driveItem == null)
                    {
                        

                        if (createDrive)
                        {
                            transactionManager.ClearItems();
                            driveItem = new clsOntologyItem
                            {
                                GUID = localConfig.Globals.NewGUID,
                                Name = driveName,
                                GUID_Parent = localConfig.OItem_class_drive.GUID,
                                Type = localConfig.Globals.Type_Object
                            };

                            result = transactionManager.do_Transaction(driveItem);

                            if (result.GUID == localConfig.Globals.LState_Success.GUID)
                            {
                                var driveToServer = relationConfigurator.Rel_ObjectRelation(driveItem, localConfig.Globals.OItem_Server, localConfig.OItem_relationtype_is_subordinated);

                                result = transactionManager.do_Transaction(driveToServer);

                                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                                {
                                    transactionManager.rollback();
                                }
                            }
                        }
                        else
                        {
                            result = localConfig.Globals.LState_Nothing.Clone();
                        }
                    }

                    if (result.GUID == localConfig.Globals.LState_Success.GUID && driveItem == null)
                    {
                        result = localConfig.Globals.LState_Error.Clone();
                    }

                    if (driveItem != null)
                    {
                        result.add_OItem(driveItem);
                    }
                }
            }
            else
            {
                result = localConfig.Globals.LState_Error.Clone();
            }

            return result;


        }

        public clsOntologyItem GetShareFromPath(string path, bool createShare, clsOntologyItem oItemServer = null)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            if (PathContainsServer(path))
            {
                var folders = path.Split('\\');
                if (oItemServer == null)
                {
                    result = GetServerFromPath(path, createShare);
                }
                
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    if (oItemServer == null)
                    {
                        oItemServer = result.OList_Rel.First().Clone();
                    }
                    
                    
                    if (folders.Length>3)
                    {
                        var shareName = folders[3];
                        var searchShare = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Object = oItemServer.GUID,
                                Name_Other = shareName,
                                ID_Parent_Other = localConfig.OItem_class_folder.GUID,
                                ID_RelationType = localConfig.OItem_relationtype_fileshare.GUID
                            }
                        };

                        result = dbConnectorShare.GetDataObjectRel(searchShare);

                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var shareItem = dbConnectorShare.ObjectRels.Where(share => share.Name_Other.ToLower() == shareName.ToLower()).Select(share => new clsOntologyItem
                            {
                                GUID = share.ID_Other,
                                Name = share.Name_Other,
                                GUID_Parent = share.ID_Parent_Other,
                                Type = share.Ontology
                            }).FirstOrDefault();

                            if (shareItem != null)
                            {
                                result.add_OItem(shareItem);
                            }
                            else
                            {
                                if (createShare)
                                {
                                    shareItem = new clsOntologyItem
                                    {
                                        GUID = localConfig.Globals.NewGUID,
                                        Name = shareName,
                                        GUID_Parent = localConfig.OItem_class_folder.GUID,
                                        Type = localConfig.Globals.Type_Object
                                    };

                                    transactionManager.ClearItems();

                                    result = transactionManager.do_Transaction(shareItem);

                                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                                    {
                                        var relServerToShare = relationConfigurator.Rel_ObjectRelation(oItemServer, shareItem, localConfig.OItem_relationtype_fileshare);

                                        result = transactionManager.do_Transaction(relServerToShare);
                                        if (result.GUID == localConfig.Globals.LState_Error.GUID)
                                        {
                                            transactionManager.rollback();
                                        }

                                    }
                                }
                                else
                                {
                                    result = localConfig.Globals.LState_Nothing.Clone();
                                }

                                if (result.GUID == localConfig.Globals.LState_Success.GUID && shareItem == null)
                                {
                                    result = localConfig.Globals.LState_Error.Clone();
                                }

                                if (shareItem != null)
                                {
                                    result.add_OItem(shareItem);
                                }

                            }
                        }
                        
                    }
                    else
                    {
                        result = localConfig.Globals.LState_Nothing.Clone();
                    }
                }

            }

            return result;
        }

        public clsOntologyItem GetServerFromPath(string path, bool createServer)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            if (PathContainsServer(path))
            {
                var folders = path.Split('\\');
                var serverName = folders[2];
                var searchServer = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            Name = serverName,
                            GUID_Parent = localConfig.OItem_class_server.GUID
                        }
                    };

                result = dbConnectorServer.GetDataObjects(searchServer);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var oItemServer = dbConnectorServer.Objects1.FirstOrDefault(serverItem => serverItem.Name.ToLower() == serverName.ToLower());

                    if (oItemServer == null)
                    {
                        if (createServer)
                        {
                            transactionManager.ClearItems();

                            oItemServer = new clsOntologyItem
                            {
                                GUID = localConfig.Globals.NewGUID,
                                Name = serverName,
                                GUID_Parent = localConfig.OItem_class_server.GUID,
                                Type = localConfig.Globals.Type_Object
                            };

                            result = transactionManager.do_Transaction(oItemServer);

                        }
                        else
                        {
                            result = localConfig.Globals.LState_Nothing.Clone();
                        }
                    }

                    if (result.GUID == localConfig.Globals.LState_Success.GUID && oItemServer == null)
                    {
                        result = localConfig.Globals.LState_Error.Clone();
                    }

                    if (oItemServer != null)
                    {
                        result.add_OItem(oItemServer);
                    }
                    
                }
            }
            else
            {
                result = localConfig.Globals.LState_Error.Clone();
            }

            return result;
        }

        public FileWorkManager(Globals globals)
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        public FileWorkManager()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            dbConnectorBlob = new OntologyModDBConnector(localConfig.Globals);
            dbConnectorServer = new OntologyModDBConnector(localConfig.Globals);
            dbConnectorShare = new OntologyModDBConnector(localConfig.Globals);
            dbConnectorDrive = new OntologyModDBConnector(localConfig.Globals);
            dbConnectorFolder = new OntologyModDBConnector(localConfig.Globals);
            dbConnectorFileSystemObject = new OntologyModDBConnector(localConfig.Globals);
            transactionManager = new clsTransaction(localConfig.Globals);
            relationConfigurator = new clsRelationConfig(localConfig.Globals);
            MediaStoreConnector = new MediaStoreConnector(localConfig);
        }
    }
}
