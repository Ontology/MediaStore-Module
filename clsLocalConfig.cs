﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using OntologyClasses.Interfaces;
using System.Runtime.InteropServices;

namespace MediaStore_Module
{
    public class clsLocalConfig : ILocalConfig
    {
    private const string cstrID_Ontology = "54e014687c674b0e816860e46793dafa";
    private ImportWorker objImport;

    public Globals Globals { get; set; }

    private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
    public clsOntologyItem OItem_BaseConfig { get; set; }

    private OntologyModDBConnector objDBLevel_Config1;
    private OntologyModDBConnector objDBLevel_Config2;

        // AttributeTypes
    public clsOntologyItem OItem_attributetype_blob { get; set; }
        public clsOntologyItem OItem_attributetype_hash { get; set; }
        public clsOntologyItem OItem_attributetype_datetimestamp__create_ { get; set; }

        // Classes
        public clsOntologyItem OItem_class_file { get; set; }
        public clsOntologyItem OItem_class_folder { get; set; }
        public clsOntologyItem OItem_class_server { get; set; }
        public clsOntologyItem OItem_class_drive { get; set; }
        public clsOntologyItem OItem_class_path { get; set; }

        // RelationTypes
        public clsOntologyItem OItem_relationtype_fileshare { get; set; }
        public clsOntologyItem OItem_relationtype_is_subordinated { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_source { get; set; }
        public clsOntologyItem OItem_relationtype_watch { get; set; }

        // Objects
        public clsOntologyItem OItem_object_baseconfig { get; set; }

        private void get_Data_DevelopmentConfig()
    {
        var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

        var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
        if (objOItem_Result.GUID == Globals.LState_Success.GUID)
        {
            if (objDBLevel_Config1.ObjectRels.Any())
            {

                objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                }).ToList();

                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingClass.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingObject.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                }));

                objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                {
                    if (!objDBLevel_Config2.ObjectRels.Any())
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }
            }
            else
            {
                throw new Exception("Config-Error");
            }

        }

    }

    public clsLocalConfig()
    {
        Globals = new Globals();
        set_DBConnection();
        get_Config();
    }

    public clsLocalConfig(Globals Globals)
    {
        this.Globals = Globals;
        set_DBConnection();
        get_Config();
    }

    private void set_DBConnection()
    {
        objDBLevel_Config1 = new OntologyModDBConnector(Globals);
        objDBLevel_Config2 = new OntologyModDBConnector(Globals);
        objImport = new ImportWorker(Globals);
    }

    private void get_Config()
    {
        try
        {
            get_Data_DevelopmentConfig();
            get_Config_AttributeTypes();
            get_Config_RelationTypes();
            get_Config_Classes();
            get_Config_Objects();
        }
        catch (Exception ex)
        {
                var objAssembly = Assembly.GetExecutingAssembly();
                AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                var strTitle = "Unbekannt";
                if (objCustomAttributes.Length == 1)
                {
                    strTitle = objCustomAttributes.First().Title;
                }
                
                var objOItem_Result = objImport.ImportTemplates(objAssembly);
                if (objOItem_Result.GUID != Globals.LState_Error.GUID)
                {
                    get_Data_DevelopmentConfig();
                    get_Config_AttributeTypes();
                    get_Config_RelationTypes();
                    get_Config_Classes();
                    get_Config_Objects();
                }
                else
                {
                    throw new Exception("Config not importable");
                }
                
            }
    }

    public void ImportTemplates()
        {
            var objAssembly = Assembly.GetExecutingAssembly();
            AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            var strTitle = "Unbekannt";
            if (objCustomAttributes.Length == 1)
            {
                strTitle = objCustomAttributes.First().Title;
            }

            var objOItem_Result = objImport.ImportTemplates(objAssembly);
            if (objOItem_Result.GUID != Globals.LState_Error.GUID)
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            else
            {
                throw new Exception("Config not importable");
            }
        }
    private void get_Config_AttributeTypes()
    {
            var objOList_attributetype_datetimestamp__create_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                 where objOItem.ID_Object == cstrID_Ontology
                                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                 where objRef.Name_Object.ToLower() == "attributetype_datetimestamp__create_".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                                 select objRef).ToList();

            if (objOList_attributetype_datetimestamp__create_.Any())
            {
                OItem_attributetype_datetimestamp__create_ = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_datetimestamp__create_.First().ID_Other,
                    Name = objOList_attributetype_datetimestamp__create_.First().Name_Other,
                    GUID_Parent = objOList_attributetype_datetimestamp__create_.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attributetype_hash = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "attributetype_hash".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                               select objRef).ToList();

            if (objOList_attributetype_hash.Any())
            {
                OItem_attributetype_hash = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_hash.First().ID_Other,
                    Name = objOList_attributetype_hash.First().Name_Other,
                    GUID_Parent = objOList_attributetype_hash.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attributetype_blob = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "attributetype_blob".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                           select objRef).ToList();

        if (objOList_attributetype_blob.Any())
        {
            OItem_attributetype_blob = new clsOntologyItem()
            {
                GUID = objOList_attributetype_blob.First().ID_Other,
                Name = objOList_attributetype_blob.First().Name_Other,
                GUID_Parent = objOList_attributetype_blob.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_RelationTypes()
    {
            var objOList_relationtype_watch = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_watch".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

            if (objOList_relationtype_watch.Any())
            {
                OItem_relationtype_watch = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_watch.First().ID_Other,
                    Name = objOList_relationtype_watch.First().Name_Other,
                    GUID_Parent = objOList_relationtype_watch.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_source = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "relationtype_belonging_source".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                          select objRef).ToList();

            if (objOList_relationtype_belonging_source.Any())
            {
                OItem_relationtype_belonging_source = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_source.First().ID_Other,
                    Name = objOList_relationtype_belonging_source.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_source.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_fileshare = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_fileshare".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

            if (objOList_relationtype_fileshare.Any())
            {
                OItem_relationtype_fileshare = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_fileshare.First().ID_Other,
                    Name = objOList_relationtype_fileshare.First().Name_Other,
                    GUID_Parent = objOList_relationtype_fileshare.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_is_subordinated = (from objOItem in objDBLevel_Config1.ObjectRels
                                                         where objOItem.ID_Object == cstrID_Ontology
                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                         where objRef.Name_Object.ToLower() == "relationtype_is_subordinated".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                         select objRef).ToList();

            if (objOList_relationtype_is_subordinated.Any())
            {
                OItem_relationtype_is_subordinated = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_is_subordinated.First().ID_Other,
                    Name = objOList_relationtype_is_subordinated.First().Name_Other,
                    GUID_Parent = objOList_relationtype_is_subordinated.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }
        }

    private void get_Config_Objects()
    {
            var objOList_object_baseconfig = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "object_baseconfig".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

            if (objOList_object_baseconfig.Any())
            {
                OItem_object_baseconfig = new clsOntologyItem()
                {
                    GUID = objOList_object_baseconfig.First().ID_Other,
                    Name = objOList_object_baseconfig.First().Name_Other,
                    GUID_Parent = objOList_object_baseconfig.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }
        }

    private void get_Config_Classes()
    {
            var objOList_class_path = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "class_path".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

            if (objOList_class_path.Any())
            {
                OItem_class_path = new clsOntologyItem()
                {
                    GUID = objOList_class_path.First().ID_Other,
                    Name = objOList_class_path.First().Name_Other,
                    GUID_Parent = objOList_class_path.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_file = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "class_file".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

            if (objOList_class_file.Any())
            {
                OItem_class_file = new clsOntologyItem()
                {
                    GUID = objOList_class_file.First().ID_Other,
                    Name = objOList_class_file.First().Name_Other,
                    GUID_Parent = objOList_class_file.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_folder = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "class_folder".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

            if (objOList_class_folder.Any())
            {
                OItem_class_folder = new clsOntologyItem()
                {
                    GUID = objOList_class_folder.First().ID_Other,
                    Name = objOList_class_folder.First().Name_Other,
                    GUID_Parent = objOList_class_folder.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_server = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "class_server".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

            if (objOList_class_server.Any())
            {
                OItem_class_server = new clsOntologyItem()
                {
                    GUID = objOList_class_server.First().ID_Other,
                    Name = objOList_class_server.First().Name_Other,
                    GUID_Parent = objOList_class_server.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_drive = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "class_drive".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

            if (objOList_class_drive.Any())
            {
                OItem_class_drive = new clsOntologyItem()
                {
                    GUID = objOList_class_drive.First().ID_Other,
                    Name = objOList_class_drive.First().Name_Other,
                    GUID_Parent = objOList_class_drive.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }
        }

        public string IdLocalConfig
        {
            get
            {
                var attributeItem = Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(attribute => attribute.GetType() == typeof(GuidAttribute));
                if (attributeItem != null)
                {
                    return ((GuidAttribute)attributeItem).Value;
                }
                else
                {
                    return null;
                }
            }
        }
}

}