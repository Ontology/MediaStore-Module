﻿using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MediaStore_Module
{
    public class MediaStoreConnector : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbConnectorMedia;
        private OntologyModDBConnector dbConnectorHash;
        private OntologyModDBConnector dbConnectorDel;
        private OntologyModDBConnector dbConnectorLastWriteTime;
        private OntologyModDBConnector dbConnectorFile;

        private clsRelationConfig relationConfigurator;
        private clsTransaction transactionManager;

        private object objectLocker = new object();

        private ByteState byteState;
        public ByteState ByteState
        {
            get
            {
                lock(objectLocker)
                {
                    return byteState;
                }
                
            }
            set
            {
                lock(objectLocker)
                {
                    byteState = value;
                }
                
                RaisePropertyChanged(nameof(ByteState));
            }
        }

        public string MediaPath
        {
            get; private set;
        }

        public bool IsMediaPathSet
        {
            get
            {
                return !string.IsNullOrEmpty(MediaPath);
            }
        }

        public FileInfo FileInfo { get; set; }

        public string MediaWatcherPath
        {
            get; private set;
        }

        public bool IsMediaWatcherPathSet
        {
            get
            {
                return !string.IsNullOrEmpty(MediaWatcherPath);
            }
        }

        public clsOntologyItem DelMedia(clsOntologyItem oItemMedia)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            try
            {
                if (File.Exists(GetMediaBlobPath(oItemMedia)))
                {
                    File.Delete(GetMediaBlobPath(oItemMedia));
                }

                var relOAtt_File_Blob = RelFileMedia(oItemMedia, true);

                transactionManager.ClearItems();
                transactionManager.do_Transaction(relOAtt_File_Blob, true, true);
            }
            catch (Exception ex)
            {
                result = localConfig.Globals.LState_Error.Clone();
            }

            return result;
        }

        public clsObjectAtt RelFileMedia(clsOntologyItem oItemFile, bool managedMedia)
        {
            return relationConfigurator.Rel_ObjectAttribute(oItemFile, localConfig.OItem_attributetype_blob, managedMedia);
        }

        public string GetSubFolderPath(string guidFile)
        {
            var subFolder1 = guidFile.Substring(0, 2);
            var subFolder2 = guidFile.Substring(2, 2);

            return subFolder1 + Path.DirectorySeparatorChar + subFolder2;
        }

        public clsOntologyItem GetMediaPath()
        {
            MediaPath = "";
            var searchPath = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = localConfig.OItem_object_baseconfig.GUID,
                    ID_Parent_Other = localConfig.OItem_class_path.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID
                }
            };

            var result = dbConnectorMedia.GetDataObjectRel(searchPath);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (dbConnectorMedia.ObjectRels.Any())
                {
                    MediaPath = dbConnectorMedia.ObjectRels.First().Name_Other;
                }
                else
                {
                    result = localConfig.Globals.LState_Nothing.Clone();
                }
            }

            return result;
        }

        public clsOntologyItem GetMediaWatcherPath()
        {
            MediaWatcherPath = "";
            var searchPath = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = localConfig.OItem_object_baseconfig.GUID,
                    ID_Parent_Other = localConfig.OItem_class_path.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_watch.GUID
                }
            };

            var result = dbConnectorMedia.GetDataObjectRel(searchPath);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (dbConnectorMedia.ObjectRels.Any())
                {
                    MediaWatcherPath = dbConnectorMedia.ObjectRels.First().Name_Other;
                }
                else
                {
                    result = localConfig.Globals.LState_Nothing.Clone();
                }
            }

            return result;
        }

        public clsOntologyItem ComputerHashes(clsOntologyItem oItemFile = null)
        {
            var searchFiles = new List<clsObjectAtt>
            {
                new clsObjectAtt
                {
                    ID_AttributeType = localConfig.OItem_attributetype_blob.GUID,
                    ID_Class = localConfig.OItem_class_file.GUID,
                    ID_Object = oItemFile != null ? oItemFile.GUID : null,
                    Val_Bit = true
                }
            };

            var result = dbConnectorHash.GetDataObjectAtt(searchFiles,doIds:true);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var objectAttsToSave = dbConnectorHash.ObjAtts.Select(objAtt =>
                {
                    var pathFile = GetMediaBlobPath(objAtt.ID_Object);

                    if (File.Exists(pathFile))
                    {
                        var hash = GetHashOfFile(pathFile);
                        if (hash == null)
                        {
                            return null;
                        }

                        var fileItem = new clsOntologyItem
                        {
                            GUID = objAtt.ID_Object,
                            GUID_Parent = objAtt.ID_Class,
                            Type = localConfig.Globals.Type_Object
                        };

                        objAtt.Val_String = hash;

                        return objAtt;
                    }
                    return null;

                }).Where(objAtt => objAtt != null).ToList();

                result = dbConnectorHash.DelObjectAtts(dbConnectorHash.ObjAtts);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    result = dbConnectorHash.SaveObjAtt(objectAttsToSave);
                }
            }

            return result;
        }

        public string GetHasOfBytes(byte[] bytes)
        {
            try
            {
                var md5Hash = new System.Security.Cryptography.MD5CryptoServiceProvider();
                var byteMD5 = md5Hash.ComputeHash(bytes);
                return BitConverter.ToString(byteMD5).Replace("-", "");

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GetHashOfFile(string path)
        {
            
            try
            {
                var md5Hash = new System.Security.Cryptography.MD5CryptoServiceProvider();
                using (var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    var byteMD5 = md5Hash.ComputeHash(fileStream);
                    return BitConverter.ToString(byteMD5).Replace("-", "");
                }
            }
            catch(Exception ex)
            {
                return null;
            }

                
        }

        public clsOntologyItem IsFilePresent(string pathFile)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var hash = GetHashOfFile(pathFile);
            if (!string.IsNullOrEmpty(hash))
            {
                result = GetFileOfHash(hash);
            }
            else
            {
                result = localConfig.Globals.LState_Error.Clone();
            }


            return result;
        }
        public clsOntologyItem SaveBytesToManagedMedia(clsOntologyItem oItemFile, byte[] bytes)
        {
            var result = localConfig.Globals.LState_Error.Clone();
            if (IsMediaPathSet)
            {
                var hash = GetHasOfBytes(bytes);
                result = GetFileOfHash(hash);
                if (result.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    result = localConfig.Globals.LState_Success.Clone();
                    var pathFileDst = GetMediaBlobPath(oItemFile);

                    var pathFileTmp = pathFileDst + ".tmp";
                    var pathFileDel = pathFileDst + ".del";
                    var pathFileBackUp = pathFileDst + ".bak";

                    if (File.Exists(pathFileDst))
                    {


                        try
                        {
                            if (File.Exists(pathFileTmp))
                            {
                                File.Delete(pathFileTmp);
                            }

                            if (File.Exists(pathFileDel))
                            {
                                File.Delete(pathFileDel);
                            }

                            int fileId = 1;
                            while (File.Exists(pathFileBackUp))
                            {
                                pathFileBackUp = pathFileDst + "_" + fileId + ".bak";
                                fileId++;
                            }
                            File.Move(pathFileDst, pathFileBackUp);




                        }
                        catch (Exception ex)
                        {
                            result = localConfig.Globals.LState_Error.Clone();
                        }

                    }

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        try
                        {
                            
                            Directory.CreateDirectory(Path.GetDirectoryName(pathFileTmp));

                            using (var streamWriter = new FileStream(pathFileTmp, FileMode.CreateNew, FileAccess.Write))
                            {
                                int byteStart = 0;
                                int byteCount = bytes.Length;
                                int byteStep = 5000;
                                int bytesLeft = byteCount - byteStart;
                                int bytesWritten = 0;

                                while (bytesLeft > 0)
                                {
                                    if (byteStep > bytesLeft)
                                    {
                                        byteStep = bytesLeft;
                                    }

                                    streamWriter.Write(bytes, byteStart, byteStep);
                                    bytesWritten += byteStep;

                                    ByteState = new ByteState
                                    {
                                        ByteCount = byteCount,
                                        BytesDone = bytesWritten
                                    };

                                    byteStart += byteStep;
                                    bytesLeft = bytesLeft - byteStep;
                                };



                                
                            }

                            File.Move(pathFileTmp, pathFileDst);

                            transactionManager.ClearItems();
                            var relFileToLastWriteTime = relationConfigurator.Rel_ObjectAttribute(oItemFile, localConfig.OItem_attributetype_datetimestamp__create_, DateTime.Now);

                            result = transactionManager.do_Transaction(relFileToLastWriteTime, true);

                            if (result.GUID == localConfig.Globals.LState_Success.GUID)
                            {
                                var relFileToManagedMedia = RelFileMedia(oItemFile, true);
                                result = transactionManager.do_Transaction(relFileToManagedMedia, true);

                                if (result.GUID != localConfig.Globals.LState_Success.GUID)
                                {
                                    File.Delete(pathFileDst);
                                    if (File.Exists(pathFileBackUp))
                                    {
                                        File.Move(pathFileBackUp, pathFileDst);
                                    }
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            result = localConfig.Globals.LState_Error.Clone();
                        }



                    }


                }


            }


            return result;
        }

        public clsOntologyItem SaveFileToManagedMedia(clsOntologyItem oItemFile, string pathFile)
        {
            var result = localConfig.Globals.LState_Error.Clone();
            if (IsMediaPathSet)
            {
                var hash = GetHashOfFile(pathFile);
                result = GetFileOfHash(hash);
                if (result.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    result = localConfig.Globals.LState_Success.Clone();
                    var pathFileDst = GetMediaBlobPath(oItemFile);

                    var pathFileTmp = pathFileDst + ".tmp";
                    var pathFileDel = pathFileDst + ".del";
                    var pathFileBackUp = pathFileDst + ".bak";

                    if (File.Exists(pathFileDst))
                    {
                            

                        try
                        {
                            if (File.Exists(pathFileTmp))
                            {
                                File.Delete(pathFileTmp);
                            }

                            if (File.Exists(pathFileDel))
                            {
                                File.Delete(pathFileDel);
                            }

                            int fileId = 1;
                            while (File.Exists(pathFileBackUp))
                            {
                                pathFileBackUp = pathFileDst + "_" + fileId + ".bak";
                                fileId++;
                            }
                            File.Move(pathFileDst, pathFileBackUp);

                                


                        }
                        catch (Exception ex)
                        {
                            result = localConfig.Globals.LState_Error.Clone();
                        }

                    }

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        try
                        {
                            FileInfo = new FileInfo(pathFile);
                            Directory.CreateDirectory(Path.GetDirectoryName(pathFileTmp));
                            using (var streamReader = new FileStream(pathFile, FileMode.Open, FileAccess.Read))
                            {
                                using (var streamWriter = new FileStream(pathFileTmp, FileMode.CreateNew, FileAccess.Write))
                                {
                                    streamReader.CopyTo(streamWriter);
                                }
                            }

                            File.Move(pathFileTmp, pathFileDst);

                            transactionManager.ClearItems();
                            var relFileToLastWriteTime = relationConfigurator.Rel_ObjectAttribute(oItemFile, localConfig.OItem_attributetype_datetimestamp__create_, FileInfo.LastWriteTime);

                            result = transactionManager.do_Transaction(relFileToLastWriteTime, true);

                            if (result.GUID == localConfig.Globals.LState_Success.GUID)
                            {
                                var relFileToManagedMedia = RelFileMedia(oItemFile, true);
                                result = transactionManager.do_Transaction(relFileToManagedMedia, true);

                                if (result.GUID != localConfig.Globals.LState_Success.GUID)
                                {
                                    File.Delete(pathFileDst);
                                    if (File.Exists(pathFileBackUp))
                                    {
                                        File.Move(pathFileBackUp, pathFileDst);
                                    }
                                }
                            }


                        }
                        catch(Exception ex)
                        {
                            result = localConfig.Globals.LState_Error.Clone();
                        }
                            
                                

                    }
                    
                    
                }
                
                
            }
            

            return result;
        }

        public Stream GetManagedMediaStream(clsOntologyItem oItemFile, bool useFileSync = false)
        {
            var result = localConfig.Globals.LState_Error.Clone();

            if (IsMediaPathSet)
            {
                var pathFileSrc = GetMediaBlobPath(oItemFile);
                try
                {
                    if (useFileSync)
                    {
                        if (useFileSync)
                        {
                            result = OpenLinkFile(pathFileSrc);
                            if (result.GUID == localConfig.Globals.LState_Error.GUID) return null;
                        }
                    }
                    return new FileStream(pathFileSrc, FileMode.Open, FileAccess.ReadWrite);
                }
                catch (Exception ex)
                {
                    return null;
                }

            }

            
            return null;
        }

        private clsOntologyItem OpenLinkFile(string pathSrc)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            var pathSrcLd = pathSrc + ".ld";
            var checkCount = 0;
            var checkMax = 10;
            if (File.Exists(pathSrcLd))
            {
                var fileInfo1 = new FileInfo(pathSrc);
                var fileInfo2 = new FileInfo(pathSrcLd);
                var fileSize1 = fileInfo1.Length;

                if (fileInfo1.Length == fileInfo2.Length)
                {
                    var processInfo = new ProcessStartInfo();
                    processInfo.FileName = pathSrc;
                    var process = new Process();
                    process.StartInfo = processInfo;
                    try
                    {
                        process.Start();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                while (fileInfo1.Length == fileInfo2.Length)
                {
                    if (checkCount > checkMax)
                    {
                        return localConfig.Globals.LState_Error.Clone();
                    }


                    fileInfo1 = new FileInfo(pathSrc);
                    fileInfo2 = new FileInfo(pathSrcLd);
                    if (fileInfo1.Length == fileSize1)
                    {
                        checkCount++;
                    }
                    System.Threading.Thread.Sleep(500);
                }

            }

            return result;
        }

        public clsOntologyItem SaveManagedMediaToFile(clsOntologyItem oItemFile, string pathFile, bool overwrite = false, bool useFileSync = false)
        {
            var result = localConfig.Globals.LState_Error.Clone();
            if (IsMediaPathSet)
            {
                result = localConfig.Globals.LState_Success.Clone();

                var pathSrc = GetMediaBlobPath(oItemFile);
                var pathDstDel = pathFile + ".del";

                pathFile = Environment.ExpandEnvironmentVariables(pathFile);

                try
                {
                    var pathDirDst = Path.GetDirectoryName(pathFile);

                    if (!Directory.Exists(pathDirDst))
                    {
                        Directory.CreateDirectory(pathDirDst);
                    }

                    if (File.Exists(pathFile) && overwrite)
                    {
                        File.Move(pathFile, pathDstDel);
                    }
                    else if (File.Exists(pathFile) && !overwrite)
                    {
                        result = localConfig.Globals.LState_Relation.Clone();
                    }

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        if (useFileSync)
                        {
                           result =  OpenLinkFile(pathSrc);
                           if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;
                        }
                        using (var streamReader = new FileStream(pathSrc, FileMode.Open, FileAccess.Read))
                        {
                            using (var streamWriter = new FileStream(pathFile, FileMode.OpenOrCreate, FileAccess.Write))
                            {
                                streamReader.CopyTo(streamWriter);
                            }
                        }

                        var searchLastWriteDate = new List<clsObjectAtt>
                        {
                            new clsObjectAtt
                            {
                                ID_Object = oItemFile.GUID,
                                ID_AttributeType = localConfig.OItem_attributetype_datetimestamp__create_.GUID
                            }
                        };

                        result = dbConnectorLastWriteTime.GetDataObjectAtt(searchLastWriteDate, doIds: true);

                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var lastWriteDate = dbConnectorLastWriteTime.ObjAttsId.FirstOrDefault();

                            if (lastWriteDate != null)
                            {
                                var fileInfo = new FileInfo(pathFile);
                                fileInfo.LastWriteTime = lastWriteDate.Val_Datetime.Value;
                            }

                            if (File.Exists(pathDstDel))
                            {
                                File.Delete(pathDstDel);
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    if (File.Exists(pathDstDel))
                    {
                        File.Move(pathDstDel, pathFile);
                    }
                    result = localConfig.Globals.LState_Error.Clone();
                }
            }

            return result;
        }

        public clsOntologyItem GetHashOfManagedMedia(clsOntologyItem oItemFile)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            var filePath = GetMediaBlobPath(oItemFile);

            if (!string.IsNullOrEmpty(filePath))
            {
                var hash = GetHashOfFile(filePath);

                if (!string.IsNullOrEmpty(hash))
                {
                    result.Additional1 = hash;
                }
                else
                {
                    result = localConfig.Globals.LState_Error.Clone();
                }

            }
            else
            {
                result = localConfig.Globals.LState_Error.Clone();
            }

            return result;
        }

        public clsOntologyItem SaveHashOfManagedMedia(clsOntologyItem oItemFile)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            var filePath = GetMediaBlobPath(oItemFile);

            if (!string.IsNullOrEmpty(filePath))
            {
                var hash = GetHashOfFile(filePath);

                if (!string.IsNullOrEmpty(hash))
                {
                    var relHash = relationConfigurator.Rel_ObjectAttribute(oItemFile, localConfig.OItem_attributetype_hash, hash);
                    transactionManager.ClearItems();
                    result = transactionManager.do_Transaction(relHash, true);
                }
                else
                {
                    result = localConfig.Globals.LState_Error.Clone();
                }
                
            }
            else
            {
                result = localConfig.Globals.LState_Error.Clone();
            }

            return result;
        }

        public clsOntologyItem CompareFiles(string pathFileSrc, string pathFileDst)
        {
            var hashSrc = GetHashOfFile(pathFileSrc);
            var hashDst = GetHashOfFile(pathFileDst);

            if (hashSrc != hashDst)
            {
                var result = localConfig.Globals.LState_Update.Clone();
                result.Additional1 = hashSrc;
                result.Additional2 = hashDst;
                return result;
            }
            else
            {
                var result = localConfig.Globals.LState_Success.Clone();
                result.Additional1 = hashSrc;
                result.Additional2 = hashDst;
                return result;
            }
        }

        public clsOntologyItem SearchFileByIdentity(string pathFile)
        {
            var result = GetGuidOfFSFile(pathFile);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var searchFile = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = result.Additional1
                    }
                };
                var fileItem = dbConnectorFile.GetOItem(result.Additional1, localConfig.Globals.Type_Object);

                if (fileItem != null)
                {
                    result = localConfig.Globals.LState_Success.Clone();
                    result.add_OItem(fileItem);
                }
            }
            else
            {
                result = localConfig.Globals.LState_Error.Clone();
            }

            return result;
        }

        public clsOntologyItem GetGuidOfFSFile(string pathFile)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var shellObject = ShellObject.FromParsingName(pathFile);

            if (shellObject != null)
            {
                var propertyItem = shellObject.Properties.GetProperty(SystemProperties.System.Comment);

                if (propertyItem.ValueAsObject != null)
                {
                    var propertyString = propertyItem.ValueAsObject.ToString();
                    var regex = new Regex("@OGUID:\\S+@");
                    var match = regex.Match(propertyString);
                    if (match.Success)
                    {
                        propertyString = propertyString.Replace("@", "");
                        var guid = propertyString.Replace("OGUID:", "");

                        if (localConfig.Globals.is_GUID(guid))
                        {
                            result.Additional1 = guid;
                        }
                        else
                        {
                            result = localConfig.Globals.LState_Relation.Clone();
                        }
                    }
                    else
                    {
                        result = localConfig.Globals.LState_Nothing.Clone();
                    }
                    
                }
                else
                {
                    result = localConfig.Globals.LState_Nothing.Clone();
                }
            }
            else
            {
                result = localConfig.Globals.LState_Error.Clone();
            }

            return result;
        }

        public clsOntologyItem WriteIdentityToFile(clsOntologyItem oItemFile, string pathFile)
        {
            var result = GetGuidOfFSFile(pathFile);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (result.Additional1 != oItemFile.GUID)
                {
                    result = localConfig.Globals.LState_Relation.Clone();
                }

            }
            else if (result.GUID == localConfig.Globals.LState_Nothing.GUID)
            {
                var identityString = "@OGUID:" + oItemFile.GUID + "@";
                var shellObject = ShellObject.FromParsingName(pathFile);

                if (shellObject != null)
                {
                    try
                    {
                        using (var propertyWriter = shellObject.Properties.GetPropertyWriter())
                        {
                            propertyWriter.WriteProperty(SystemProperties.System.Comment, identityString);
                        }
 
                    }
                    catch (Exception ex)
                    {
                        result = localConfig.Globals.LState_Error.Clone();
                    }
                }
                else
                {
                    result = localConfig.Globals.LState_Error.Clone();
                }

                shellObject.Dispose();
            }

            return result;
        }

        public clsOntologyItem GetFileOfHash(string hash)
        {
            var searchObjectAtt = new List<clsObjectAtt>
            {
                new clsObjectAtt
                {
                    ID_AttributeType = localConfig.OItem_attributetype_hash.GUID,
                    ID_Class = localConfig.OItem_class_file.GUID
                }
            };

            var result = dbConnectorHash.GetDataObjectAtt(searchObjectAtt, doIds: false);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var fileItem = dbConnectorHash.ObjAtts.Where(objAtt => objAtt.Val_String == hash).Select(objAtt => new clsOntologyItem
                {
                    GUID = objAtt.ID_Object,
                    Name = objAtt.Name_Object,
                    GUID_Parent = objAtt.ID_Class,
                    Type = localConfig.Globals.Type_Object
                }).FirstOrDefault();

                if (fileItem != null)
                {
                    result.OList_Rel = new List<clsOntologyItem>();
                    result.OList_Rel.Add(fileItem);
                }
                else
                {
                    return localConfig.Globals.LState_Nothing.Clone();
                }
            }


            return result;
        }

        public string GetMediaBlobPath(clsOntologyItem oItemMedia)
        {
            return GetMediaBlobPath(oItemMedia.GUID);
        }

        public string GetMediaBlobPath(string guidMedia)
        {
            return MediaPath + Path.DirectorySeparatorChar + GetSubFolderPath(guidMedia) + Path.DirectorySeparatorChar + guidMedia;
        }

        public MediaStoreConnector(Globals globals)
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        public MediaStoreConnector()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        public MediaStoreConnector(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {
            dbConnectorMedia = new OntologyModDBConnector(localConfig.Globals);
            dbConnectorHash = new OntologyModDBConnector(localConfig.Globals);
            dbConnectorDel = new OntologyModDBConnector(localConfig.Globals);
            dbConnectorLastWriteTime = new OntologyModDBConnector(localConfig.Globals);
            dbConnectorFile = new OntologyModDBConnector(localConfig.Globals);

            relationConfigurator = new clsRelationConfig(localConfig.Globals);
            transactionManager = new clsTransaction(localConfig.Globals);

            var result = GetMediaPath();
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = GetMediaWatcherPath();
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    throw new Exception("No Media-Path");
                }
            }
            else
            {
                throw new Exception("No Media-Path");
            }
            
        }
    }

    public class ByteState
    {
        public long ByteCount { get; set; }
        public long BytesDone { get; set; }
    }
}
